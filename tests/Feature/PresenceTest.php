<?php

namespace Tests\Feature;

use App\Models\Presence;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class PresenceTest extends TestCase
{
    use RefreshDatabase;

    private string $url = '/presences';

    public function test_request_validation()
    {
        $response = $this->postJson($this->url);
        $response->assertStatus(422);

        $response2 = $this->postJson($this->url, ['type' => 'whatever']);
        $response2->assertStatus(422);
    }

    public function test_weekdays_only()
    {
        // Travel to 2023-08-19 08:00:00
        $this->travelTo(Carbon::create(2023, 8, 19, 8));

        $response = $this->postJson($this->url, ['type' => 'in']);
        $response
            ->assertStatus(400)
            ->assertJson(['message' => 'Presence are only allowed on weekdays (Monday to Friday).']);
    }

    public function test_cannot_clock_in_before_working_hours()
    {
        // Travel to 2023-08-01 07:00:00
        $this->travelTo(Carbon::create(2023, 8, 1, 7));

        $response = $this->postJson($this->url, ['type' => 'in']);
        $response
            ->assertStatus(400)
            ->assertJson(['message' => 'Clock-in is not allowed before the designated working hours.']);
    }

    public function test_already_clock_in()
    {
        // Travel to 2023-08-01 08:00:00
        $this->travelTo(Carbon::create(2023, 8, 1, 8));
        $this->postJson($this->url, ['type' => 'in']);

        $response = $this->postJson($this->url, ['type' => 'in']);
        $response
            ->assertStatus(400)
            ->assertJson(['message' => 'You have already clock-in for today.']);
    }

    public function test_cannot_clock_out_without_clock_in()
    {
        // Travel to 2023-08-02 08:00:00
        $this->travelTo(Carbon::create(2023, 8, 2, 8));

        $response = $this->postJson($this->url, ['type' => 'out']);
        $response
            ->assertStatus(400)
            ->assertJson(['message' => 'Clock-out is not allowed before clocking in.']);
    }

    public function test_cannot_clock_out_during_working_hours()
    {
        // Travel to 2023-08-02 08:00:00
        $this->travelTo(Carbon::create(2023, 8, 2, 8));
        $this->postJson($this->url, ['type' => 'in']);

        // Travel to 2023-08-02 15:00:00
        $this->travelTo(Carbon::create(2023, 8, 2, 15));
        $response = $this->postJson($this->url, ['type' => 'out']);
        $response
            ->assertStatus(400)
            ->assertJson(['message' => 'Clock-out is not allowed during designated working hours.']);
    }

    public function test_already_clock_out()
    {
        // Travel to 2023-08-02 08:00:00
        $this->travelTo(Carbon::create(2023, 8, 2, 8));
        $this->postJson($this->url, ['type' => 'in']);

        // Travel to 2023-08-02 16:00:00
        $this->travelTo(Carbon::create(2023, 8, 2, 16));
        $this->postJson($this->url, ['type' => 'out']);

        $response = $this->postJson($this->url, ['type' => 'out']);
        $response
            ->assertStatus(400)
            ->assertJson(['message' => 'You have already clock-out for today.']);
    }

    public function test_did_not_get_performance_allowance_before_clock_out()
    {
        // Travel to 2023-08-03 08:00:00
        $this->travelTo(Carbon::create(2023, 8, 3, 8));
        $this->postJson($this->url, ['type' => 'in']);

        $dt = now();
        $presence = Presence::whereDate('date', $dt)->first();

        $this->assertNotNull($presence);
        $this->assertFalse($presence->performance_allowance);
    }

    public function test_did_not_get_performance_allowance_because_of_late()
    {
        // Travel to 2023-08-04 09:00:00
        $this->travelTo(Carbon::create(2023, 8, 4, 9));
        $this->postJson($this->url, ['type' => 'in']);

        // Travel to 2023-08-04 16:00:00
        $this->travelTo(Carbon::create(2023, 8, 4, 16));
        $this->postJson($this->url, ['type' => 'out']);

        $dt = now();
        $presence = Presence::whereDate('date', $dt)->first();

        $this->assertNotNull($presence);
        $this->assertFalse($presence->performance_allowance);
    }

    public function test_get_late_deduction()
    {
        $testData = [
            '2023-08-07 08:10:00' => 0,
            '2023-08-08 08:15:00' => 5000,
            '2023-08-09 08:25:00' => 5000,
            '2023-08-10 08:30:00' => 10000,
            '2023-08-11 08:45:00' => 10000,
            '2023-08-14 09:00:00' => 0,
            '2023-08-15 10:00:00' => 0,
        ];

        foreach ($testData as $date => $lateDeduction) {
            $dt = Carbon::createFromFormat('Y-m-d H:i:s', $date);
            $this->travelTo($dt);
            $this->postJson($this->url, ['type' => 'in']);

            $presence = Presence::whereDate('date', $dt)->first();
            $this->assertNotNull($presence);
            $this->assertEquals($lateDeduction, $presence->late_deduction_amount);
        }
    }

    public function test_perfect_presence()
    {
        // Travel to 2023-08-16 08:10:00
        $this->travelTo(Carbon::create(2023, 8, 16, 8, 10));
        $inResponse = $this->postJson($this->url, ['type' => 'in']);
        $inResponse
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('message')
                     ->has('presence')
                     ->where('message', 'Success.')
                     ->etc()
            );

        // Travel to 2023-08-16 16:00:00
        $this->travelTo(Carbon::create(2023, 8, 16, 16));
        $outResponse = $this->postJson($this->url, ['type' => 'out']);
        $outResponse
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('message')
                     ->has('presence')
                     ->where('message', 'Success.')
                     ->etc()
            );

        $dt = now();
        $presence = Presence::whereDate('date', $dt)->first();

        $this->assertNotNull($presence);
        $this->assertNotNull($presence->clock_out);
        $this->assertTrue($presence->performance_allowance);
        $this->assertEquals(0, $presence->late_deduction_amount);
    }
}
