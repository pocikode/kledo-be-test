<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class PayslipTest extends TestCase
{
    use RefreshDatabase;

    private string $url = '/payslips';

    private function get_test_data_january()
    {
        $testData = [];
        $late_15_minutes = 0;
        $late_30_minutes = 0;
        $late_60_minutes = 0;
        $without_clock_out = 0;

        $period = CarbonPeriod::create('2023-01-01', '2023-01-31');
        foreach ($period as $date) {
            if ($date->isWeekend()) {
                continue;
            }

            $testDataTmp = [
                'date' => $date,
                'late_minutes' => 0,
                'clock_out' => true,
            ];

            if ($late_15_minutes < 3) {
                $testDataTmp['late_minutes'] = 15;
                $testData[] = $testDataTmp;
                $late_15_minutes++;
                continue;
            }

            if ($late_30_minutes < 3) {
                $testDataTmp['late_minutes'] = 30;
                $testData[] = $testDataTmp;
                $late_30_minutes++;
                continue;
            }

            if ($late_60_minutes < 3) {
                $testDataTmp['late_minutes'] = 60;
                $testData[] = $testDataTmp;
                $late_60_minutes++;
                continue;
            }

            if ($without_clock_out < 3) {
                $testDataTmp['clock_out'] = false;
                $testData[] = $testDataTmp;
                $without_clock_out++;
                continue;
            }

            $testData[] = $testDataTmp;
        }

        return $testData;
    }

    public function test_request_validation()
    {
        $response = $this->postJson($this->url);
        $response->assertStatus(422);

        $response2 = $this->postJson($this->url, ['month' => 'whatever']);
        $response2->assertStatus(422);
    }

    public function test_get_payslip()
    {
        // seed data january via /presences
        $testDataJanuary = $this->get_test_data_january();
        foreach ($testDataJanuary as $testData) {
            $dt = $testData['date']->copy()->setHour(8)->setMinute(0)->setSeconds(0);
            $dt->addMinutes($testData['late_minutes']);

            $this->travelTo($dt);
            $this->postJson('/presences', ['type' => 'in']);

            if ($testData['clock_out']) {
                $dt->setHour(16)->setMinute(0);
                $this->travelTo($dt);
                $this->postJson('/presences', ['type' => 'out']);
            }
        }

        $salary = 2000000;
        $lateDeduction = -((5000*3) + (10000*3));
        $performanceAllowance = 15000 * (count($testDataJanuary) - 6);

        $response = $this->postJson($this->url, ['month' => '2023-01']);
        echo($response->getContent());
        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('month', '2023-01')
                     ->has('components.0', fn ($json) =>
                        $json->where('name', 'Gaji Pokok')
                             ->where('amount', $salary)
                     )
                     ->has('components.1', fn ($json) =>
                        $json->where('name', 'Tunjangan Kinerja')
                             ->where('amount', $performanceAllowance)
                     )
                     ->has('components.2', fn ($json) =>
                        $json->where('name', 'Potongan Keterlambatan')
                             ->where('amount', $lateDeduction)
                     )
                     ->where('take_home_pay', $salary + $performanceAllowance + $lateDeduction)
                     ->etc()
            );
    }
}
