<?php

namespace App\Http\Controllers;

use App\Http\Requests\PayslipRequest;
use App\Models\Presence;
use Carbon\Carbon;

class PayslipController extends Controller
{
    public function payslip(PayslipRequest $request)
    {
        $month = Carbon::createFromFormat('Y-m', $request->json('month'));
        $startOfMonth = $month->copy()->startOfMonth();
        $endOfMonth = $month->copy()->endOfMonth();

        $salaryAmount = config('app.main_salary');
        $performanceAllowanceTotal = 0;
        $lateDeductionTotal = 0;

        $presences = Presence::whereDate('date', '>=', $startOfMonth)
            ->whereDate('date', '<=', $endOfMonth)
            ->get();

        foreach ($presences as $presence) {
            if ($presence->performance_allowance) {
                $performanceAllowanceTotal += 15000;
            }

            $lateDeductionTotal += $presence->late_deduction_amount;
        }

        $salaryTotal = $salaryAmount + $performanceAllowanceTotal - $lateDeductionTotal;

        return response()->json([
            'month' => $request->json('month'),
            'components' => [
                [
                    'name' => 'Gaji Pokok',
                    'amount' => $salaryAmount,
                ],
                [
                    'name' => 'Tunjangan Kinerja',
                    'amount' => $performanceAllowanceTotal,
                ],
                [
                    'name' => 'Potongan Keterlambatan',
                    'amount' => -$lateDeductionTotal,
                ],
            ],
            'take_home_pay' => $salaryTotal,
        ]);
    }
}
