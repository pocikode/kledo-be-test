<?php

namespace App\Http\Controllers;

use App\Http\Requests\PresenceRequest;
use App\Models\Presence;
use Carbon\Carbon;

class PresenceController extends Controller
{
    public function presence(PresenceRequest $request)
    {
        $dt = now();
        $type = $request->json('type');
        if ($dt->isWeekend()) {
            return response_error(400, 'Presence are only allowed on weekdays (Monday to Friday).');
        }

        /** @var Presence $presence */
        $presence = Presence::whereDate('date', $dt)->first();
        $clockInTime = Carbon::createFromFormat('H:i', config('app.clock_in_time'));
        $clockOutTime = Carbon::createFromFormat('H:i', config('app.clock_out_time'));

        if ($type === 'in') {
            if ($dt < $clockInTime) {
                return response_error(400, 'Clock-in is not allowed before the designated working hours.');
            }

            if (!is_null($presence)) {
                return response_error(400, 'You have already clock-in for today.');
            }

            $presence = new Presence();
            $presence->date = $dt;
            $presence->clock_in = $dt;

            $clockInLateInMinutes = $dt->diffInMinutes($clockInTime);
            if ($clockInLateInMinutes >= 15 and $clockInLateInMinutes < 30) {
                $presence->late_deduction_amount = 5000;
            } elseif ($clockInLateInMinutes >= 30 and $clockInLateInMinutes < 60) {
                $presence->late_deduction_amount = 10000;
            } else {
                $presence->late_deduction_amount = 0;
            }

            $presence->save();
        }

        if ($type === 'out') {
            if (is_null($presence)) {
                return response_error(400, 'Clock-out is not allowed before clocking in.');
            }

            if (!is_null($presence->clock_out)) {
                return response_error(400, 'You have already clock-out for today.');
            }

            if ($dt < $clockOutTime) {
                return response_error(400, 'Clock-out is not allowed during designated working hours.');
            }

            if ($presence->clock_in_date_time->diffInMinutes($clockInTime) < 60) {
                $presence->performance_allowance = true;
            }

            $presence->clock_out = $dt;
            $presence->save();
        }

        return response()->json([
            'message' => 'Success.',
            'presence' => $presence,
        ]);
    }
}
