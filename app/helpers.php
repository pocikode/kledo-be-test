<?php

if (!function_exists('response_error')) {
    function response_error(int $code, string $message): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'message' => $message,
        ], $code);
    }
}
