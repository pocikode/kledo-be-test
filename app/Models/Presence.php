<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Presence Model
 *
 * @property int $id
 * @property string $date
 * @property string $clock_in
 * @property string $clock_out
 * @property boolean $performance_allowance
 * @property int $late_deduction_amount
 * @property Carbon $clock_in_date_time
 */
class Presence extends Model
{
    use HasFactory;

    protected $fillable = ['date', 'clock_in', 'clock_out', 'performance_allowance', 'late_deduction_amount'];

    protected $casts = ['performance_allowance' => 'boolean'];

    /**
     * Get Clock-In Date and Time
     *
     * @return Carbon
     */
    public function getClockInDateTimeAttribute(): Carbon
    {
        return Carbon::createFromFormat(
            'Y-m-d H:i:s',
            $this->date . ' ' . $this->clock_in
        );
    }
}
